# COVID19 Plot Argentina #
Este script de Python usa la librería Matplotlib para plotear la curva de casos publicada diariamente en la [cuenta de Tweeter de J. Fraire](https://twitter.com/TotinFraire).

## Información en el Gráfico ##
Para cada provincia se muestran valores de casos y fallecidos acumulados. Además, en la curva se puede apreciar los fallecimientos de cada provincia en el tiempo. Los recuperados se indican en línea punteada y el área sombreada por encima indica infectados activos. De ser necesario, hay un video explicativo para el [gráfico 1](https://youtu.be/HIYDYVt-q7Y) y otro para el [gráfico 2](https://youtu.be/hEzczlyr-y0). 

Es importante notar que los reportes provinciales pueden diferir de los expresados por nación. Más información al respecto en [este tweet](https://twitter.com/TotinFraire/status/1271228833761501184?s=20).

Los pasos para generar las gráficas diariamente son dos, y muy simples: 1) carga de datos en una planilla pública, y 2) correr el script que genera las curvas.

## 1) Fuentes y Carga de Datos ##
El script toma automáticamente los datos de la planilla de [Infomapache](https://twitter.com/infomapache) en el repositorio [Covid19arData](https://github.com/SistemasMapache/Covid19arData) y de [Jorge Aliaga](https://twitter.com/jorgeluisaliaga) en su [registro público](https://docs.google.com/spreadsheets/d/1M7uDgWSfy6z1MNbC9FP6jTNgvY7XchJ0m-BfW88SKtQ/edit?usp=sharing).

La planilla de datos de infomapache se debe cargar a mano con cada reporte diario por nosotros. Básicamente es copy-paste del último día y actualizar `nue_casosconf_diff` y `nue_fallecidos_diff` con los nuevos casos y fallecidos por provincia. Los scripts `report-parse-*.py` pueden usarse para facilitar el copy-paste de los fallecidos y casos del reporte diario en pdf. La planilla de Jorge Aliaga es cargada por él diariamente.

## 2) Generación de las Gráficas ##
Para quien quiera revisar el espíritu de la gráfica y las bases de cómo se usa Python y Matplotlib puede pasar el [éste tutorial](https://youtu.be/MEPRUsDEuA8). 

#### Instalar dependencias
(Sólo correr la 1era vez para instalar las librerías)
```bash
python3 -m pip install matplotlib imageio
```

#### Ejecucion del script
(Correr una vez que se cargaron las planillas de Infomapache y J. Aliaga)
```bash
python3 cv-ar.py
```

Al ejecutar, el script bajará los datos de las planillas y dejará en la carpeta las imágenes en alta resolución listas para subir y compartir (formato .png y .svg).

